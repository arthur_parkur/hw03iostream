import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static Scanner sc;
    private static final int PAGE_SIZE = 1800;

    public static void main(String[] args) throws IOException {
        //задача №1 читать 50 байт из файла
        //task1();

        //задача №2 читаем 3 файла как один и записываем в res.txt
        //task2();

        getPage();
    }

    public static void task1() throws IOException {
        FileInputStream in = new FileInputStream("1.txt");
        byte[] bytes = new byte[in.available()];
        in.read(bytes);
        String str = new String(bytes);
        System.out.println(str);
        in.close();
    }

    public static void task2() throws IOException {
        ArrayList<FileInputStream> fileList = new ArrayList<>();
        fileList.add(new FileInputStream("task2/1.txt"));
        fileList.add(new FileInputStream("task2/2.txt"));
        fileList.add(new FileInputStream("task2/3.txt"));

        SequenceInputStream in = new SequenceInputStream(Collections.enumeration(fileList));
        FileOutputStream out = new FileOutputStream("task2/res.txt");
        int x;
        while ((x = in.read()) != -1) {
            out.write(x);
        }
        in.close();
        out.close();
    }

    public static void getPage() throws IOException {
        //BufferedInputStream in = new BufferedInputStream(new FileInputStream("task3.txt"));
        RandomAccessFile in = new RandomAccessFile("task3.txt", "rw");
        sc = new Scanner(System.in);

        System.out.println("Введите номер страницы");

        int pageNumber;
        boolean exit = false;
        while (!exit) {
            try {
                pageNumber = sc.nextInt();
                in.seek(PAGE_SIZE * pageNumber);
                StringBuilder out = new StringBuilder();
                for (int i = 0; i < PAGE_SIZE; i++) {
                    out.append((char) in.read());
                }
                in.close();
                System.out.println(out);
                exit = true;
            } catch (InputMismatchException e) {
                exit = true;
            }

        }
    }
}
